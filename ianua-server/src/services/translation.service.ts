import {getService} from '@loopback/service-proxy';
import {inject, Provider} from '@loopback/core';
import {TranslateDataSource} from '../datasources';

export interface Translation {
  // this is where you define the Node.js methods that will be
  // mapped to REST/SOAP/gRPC operations as stated in the datasource
  // json file.
  translate(args: translateParameters):Promise<translateParameters>;
  
}

export interface translationResponse {
  result: {
    to: string;
  };
}

export interface translateParameters {
  to: string;
  from: string;
}

export class TranslationProvider implements Provider<Translation> {
  constructor(
    // translate must match the name property in the datasource json file
    @inject('datasources.translate')
    protected dataSource: TranslateDataSource = new TranslateDataSource(),
  ) {}

  value(): Promise<Translation> {
    return getService(this.dataSource);
  }
}
