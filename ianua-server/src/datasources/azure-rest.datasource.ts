import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

var subscriptionKey = '7232581536fb4153a2a5d4a7f397be08';
var endpoint = 'https://api.cognitive.microsofttranslator.com';

const config = {
  name: 'AzureRest',
  connector: 'rest',
  baseURL: endpoint,
  crud: false,
  options: {
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      'Ocp-Apim-Subscription-Key': subscriptionKey,
    },
  },
  operations: [
    {
      template: {
        method: 'POST',
        url: endpoint + '/translate?api-version=3.0',
        query: {
          to: '{^to}',
        },
        body: [{Text: '{^text}'}],
      },
      functions: {
        translate: ['to', 'text'],
      },
    },
  ],
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class AzureRestDataSource
  extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'AzureRest';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.AzureRest', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
