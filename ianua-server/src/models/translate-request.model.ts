import {Model, model, property} from '@loopback/repository';

@model()
export class TranslateRequest extends Model {
  @property({
    type: 'string',
    required: true,
  })
  text: string;

  @property({
    type: 'string',
    required: true,
  })
  to: string;


  constructor(data?: Partial<TranslateRequest>) {
    super(data);
  }
}

export interface TranslateRequestRelations {
  // describe navigational properties here
}

export type TranslateRequestWithRelations = TranslateRequest & TranslateRequestRelations;
